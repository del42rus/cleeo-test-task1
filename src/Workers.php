<?php

namespace LoadBalancer;

use ArrayIterator;
use IteratorAggregate;

class Workers implements IteratorAggregate {
    private $workers;

    public function __construct(WorkerInterface ...$workers) {
        $this->workers = $workers;
    }

    public function getIterator() {
        return new ArrayIterator($this->workers);
    }
}