<?php

namespace LoadBalancer;

abstract class AbstractWorker implements WorkerInterface
{
    private $load = 0;

    public function processTask(TaskInterface $task)
    {
        $this->load++;
    }

    public function getLoad(): int
    {
        return $this->load;
    }

    abstract public function isBusy(): bool;
}