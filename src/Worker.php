<?php

namespace LoadBalancer;

class Worker extends AbstractWorker
{
    public function isBusy(): bool
    {
        return false;
    }
}