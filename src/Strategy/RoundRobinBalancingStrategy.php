<?php

namespace LoadBalancer\Strategy;

use LoadBalancer\WorkerInterface;
use LoadBalancer\Workers;

class RoundRobinBalancingStrategy implements LoadBalancingStrategyInterface
{
    private $position = 0;

    public function getWorker(Workers $workers) : WorkerInterface
    {
        $workers = $workers->getIterator();
        $workers->seek($this->position);

        $worker = $workers->current();

        while ($worker->isBusy()) {
            $workers->next();
            $worker = $workers->current();

            if (!$worker) {
                $workers->rewind();
                $worker = $workers->current();
            }
        }

        $workers->next();

        $this->position = (int) $workers->key();

        return $worker;
    }
}