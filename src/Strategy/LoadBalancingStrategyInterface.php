<?php

namespace LoadBalancer\Strategy;

use LoadBalancer\WorkerInterface;
use LoadBalancer\Workers;

interface LoadBalancingStrategyInterface
{
    public function getWorker(Workers $workers) : WorkerInterface;
}