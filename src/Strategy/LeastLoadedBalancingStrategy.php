<?php

namespace LoadBalancer\Strategy;

use LoadBalancer\WorkerInterface;
use LoadBalancer\Workers;

class LeastLoadedBalancingStrategy implements LoadBalancingStrategyInterface
{
    public function getWorker(Workers $workers) : WorkerInterface
    {
        $leastLoadedWorker = null;

        do {
            foreach ($workers as $worker) {
                if ($worker->isBusy()) {
                    continue;
                }

                if ($leastLoadedWorker) {
                    if ($worker->getLoad() < $leastLoadedWorker->getLoad()) {
                        $leastLoadedWorker = $worker;
                    }
                } else {
                    $leastLoadedWorker = $worker;
                }
            }
        } while (!$leastLoadedWorker);

        return $leastLoadedWorker;
    }
}