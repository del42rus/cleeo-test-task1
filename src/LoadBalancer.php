<?php

namespace LoadBalancer;

use LoadBalancer\Strategy\LoadBalancingStrategyInterface;

class LoadBalancer
{
    private $workers = [];

    private $loadBalancingStrategy;

    public function __construct(Workers $workers, LoadBalancingStrategyInterface $loadBalancingStrategy)
    {
        $this->workers = $workers;
        $this->loadBalancingStrategy = $loadBalancingStrategy;
    }

    public function processTask(TaskInterface $task)
    {
        $this->loadBalancingStrategy->getWorker($this->workers)->processTask($task);
    }
}