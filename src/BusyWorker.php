<?php

namespace LoadBalancer;

class BusyWorker extends AbstractWorker
{
    public function isBusy(): bool
    {
        return true;
    }
}