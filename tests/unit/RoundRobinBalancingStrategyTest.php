<?php


use LoadBalancer\BusyWorker;
use LoadBalancer\SomeTask;
use LoadBalancer\Strategy\RoundRobinBalancingStrategy;
use LoadBalancer\Workers;
use LoadBalancer\Worker;

class RoundRobinBalancingStrategyTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    // tests
    public function testWorkerSelection()
    {
        $worker1 = new Worker();
        $worker1->processTask(new SomeTask());
        $worker1->processTask(new SomeTask());

        $worker2 = new Worker();
        $worker2->processTask(new SomeTask());

        $worker3 = new BusyWorker();
        $worker3->processTask(new SomeTask());

        $workers = new Workers($worker1, $worker2, $worker3);

        $loadBalancingStrategy = new RoundRobinBalancingStrategy();

        $worker = $loadBalancingStrategy->getWorker($workers);

        $this->assertSame($worker, $worker1);

        $worker = $loadBalancingStrategy->getWorker($workers);

        $this->assertSame($worker, $worker2);

        $worker = $loadBalancingStrategy->getWorker($workers);

        $this->assertSame($worker, $worker1);
    }
}