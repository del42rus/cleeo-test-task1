<?php

use LoadBalancer\BusyWorker;
use LoadBalancer\LoadBalancer;
use LoadBalancer\SomeTask;
use LoadBalancer\Strategy\LeastLoadedBalancingStrategy;
use LoadBalancer\Strategy\RoundRobinBalancingStrategy;
use LoadBalancer\Workers;
use LoadBalancer\Worker;

class LoadBalancerTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    // tests
    public function testLoadBalancing()
    {
        $worker1 = new Worker();
        $worker1->processTask(new SomeTask());
        $worker1->processTask(new SomeTask());

        $worker2 = new Worker();
        $worker2->processTask(new SomeTask());

        $worker3 = new BusyWorker();
        $worker3->processTask(new SomeTask());

        $workers = new Workers($worker1, $worker2, $worker3);

        $loadBalancer = new LoadBalancer($workers, new RoundRobinBalancingStrategy());

        $loadBalancer->processTask(new SomeTask());
        $loadBalancer->processTask(new SomeTask());
        $loadBalancer->processTask(new SomeTask());

        $this->assertEquals(4, $worker1->getLoad());
        $this->assertEquals(2, $worker2->getLoad());
        $this->assertEquals(1, $worker3->getLoad());

        $loadBalancer = new LoadBalancer($workers, new LeastLoadedBalancingStrategy());

        $loadBalancer->processTask(new SomeTask());
        $loadBalancer->processTask(new SomeTask());
        $loadBalancer->processTask(new SomeTask());

        $this->assertEquals(5, $worker1->getLoad());
        $this->assertEquals(4, $worker2->getLoad());
        $this->assertEquals(1, $worker3->getLoad());
    }
}