<?php

require __DIR__.'/vendor/autoload.php';

use LoadBalancer\LoadBalancer;
use LoadBalancer\Strategy\RoundRobinBalancingStrategy;
use LoadBalancer\Strategy\LeastLoadedBalancingStrategy;
use LoadBalancer\SomeTask;
use LoadBalancer\Worker;
use LoadBalancer\BusyWorker;
use LoadBalancer\Workers;

$worker1 = new Worker();
$worker1->processTask(new SomeTask());
$worker1->processTask(new SomeTask());

$worker2 = new Worker();
$worker2->processTask(new SomeTask());

$worker3 = new BusyWorker();
$worker3->processTask(new SomeTask());

$workers = new Workers($worker1, $worker2, $worker3);

$loadBalancer = new LoadBalancer($workers, new RoundRobinBalancingStrategy());

echo "Initial load of workers\n";
echo "Worker1: " . $worker1->getLoad() . "\n";
echo "Worker2: " . $worker2->getLoad() . "\n";
echo "Worker3(busy): " . $worker3->getLoad() . "\n";

$loadBalancer->processTask(new SomeTask());
$loadBalancer->processTask(new SomeTask());
$loadBalancer->processTask(new SomeTask());

echo "\nWorkers' load after Round Robin Balancing Algorithm\n";

echo "Worker1: " . $worker1->getLoad() . "\n";
echo "Worker2: " . $worker2->getLoad() . "\n";
echo "Worker3(busy): " . $worker3->getLoad() . "\n";

$loadBalancer = new LoadBalancer($workers, new LeastLoadedBalancingStrategy());

$loadBalancer->processTask(new SomeTask());
$loadBalancer->processTask(new SomeTask());
$loadBalancer->processTask(new SomeTask());

echo "\nWorkers' load after Least Loaded Algorithm\n";

echo "Worker1: " . $worker1->getLoad() . "\n";
echo "Worker2: " . $worker2->getLoad() . "\n";
echo "Worker3(busy): " . $worker3->getLoad() . "\n";