## Installation
Go to the root directory. Run the following command
```sh
$ composer install
$ composer dump-autoload
```

## Run application

```sh
$ php application.php
```

## Run Tests
Run the  command
```sh
$ php vendor/bin/codecept run unit
```